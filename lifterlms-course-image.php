
<?php


/* Lifter LMS */
add_shortcode('course-image', 'course_featured_image');
function course_featured_image($atts) {
//     global $lesson;
//     return get_the_post_thumbnail($course->ID);

	$lesson = llms_get_post( get_the_ID() );
	if ( $lesson ) {
	   $course_id = $lesson->get_parent_course();
	   
	   $background = wp_get_attachment_image_src( get_post_thumbnail_id( $course_id ), 'full' );
	   
	   $output = sprintf('<h2 class="fl-heading"><span class="fl-heading-text">%s</span></h2>', get_the_title( $course_id ) );
	   $output .=<<<javascript
<script>
jQuery(function($){
	var args = new Object(); 
	args['background-image'] = 'url({$background[0]})';
	args['background-repeat'] = 'no-repeat';
	args['background-position'] = 'center center';
	args['background-attachment'] = "scroll";
	args['background-size'] = "cover";

	$('#course-header > .fl-row-content-wrap').css(args);
});
</script>
javascript;


		return $output;
	}

}

